<?php
/**
 * Marketify child theme.
 */
function marketify_child_styles() {
    wp_enqueue_style( 'marketify-child', get_stylesheet_uri() );
}
add_action( 'wp_enqueue_scripts', 'marketify_child_styles', 999 );

function enovomusic_get_custom_player( $meta_boxes ) {
	$prefix = 'enovomusic-';

	$meta_boxes[] = array(
		'id' => 'custom_plyer',
		'title' => esc_html__( 'Player Code', 'enovomusic' ),
		'post_types' => array( 'download' ),
		'context' => 'advanced',
		'priority' => 'default',
		'autosave' => true,
		'fields' => array(
			array(
				'id' => $prefix . 'custom_plyer',
				'type' => 'textarea',
				'name' => esc_html__( '', 'enovomusic' ),
				'rows' => 10,
				'cols' => 40,
			),
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'enovomusic_get_custom_player' );
/** Place any new code below this line */
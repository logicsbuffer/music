<?php
/**
 * The template for displaying Archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Marketify
 */

get_header(); 
$enovomusic_custom_plyer = rwmb_meta('enovomusic-custom_plyer');
//echo $enovomusic_custom_plyer;
//echo do_shortcode('[zoomsounds_player type="detect" dzsap_meta_source_attachment_id="336" source="http://enovomusic.com/wp-content/uploads/edd/Be-Happy.mp3" config="default" autoplay="off" loop="off" open_in_ultibox="off" enable_likes="off" enable_views="off" play_in_footer_player="off" enable_download_button="off" download_custom_link_enable="off"]');
?>

	<?php do_action( 'marketify_entry_before' ); ?>

	<?php while ( have_posts() ) : the_post(); ?>

	<div class="container">
		<div id="content" class="site-content row">

			<div role="main" class="content-area <?php echo ! is_active_sidebar( 'sidebar-download-single' ) ? 'col-xs-12' : 'col-xs-12 col-md-8'; ?>">
				
				<?php 
				echo do_shortcode($enovomusic_custom_plyer);
				get_template_part( 'content-single', 'download' ); ?>
			</div>

			<?php get_sidebar( 'single-download' ); ?>

		</div><!-- #content -->

		<?php comments_template(); ?>

		<?php do_action( 'marketify_single_download_after' ); ?>
	</div>

	<?php endwhile; ?>

<?php get_footer(); ?>
